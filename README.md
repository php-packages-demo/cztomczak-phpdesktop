# cztomczak-phpdesktop

[cztomczak/phpdesktop](https://github.com/cztomczak/phpdesktop) Desktop GUI applications using PHP, HTML5, JavaScript and SQLite

* [*Convert Php/Symfony web app into a Windows Desktop App*
  ](https://medium.com/@lejmi.amine/convert-php-symfony-web-app-into-a-windows-desktop-app-68d8775476ec)
  2020-08 Mohamed Amine Lejmi